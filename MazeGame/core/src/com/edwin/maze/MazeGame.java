package com.edwin.maze;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.edwin.maze.Screen.GameScreen;


public class MazeGame extends Game {
	public static final int V_WIDTH = 400;
	public static final int V_HEIGHT = 280;

	public static final int WALL_BIT = 1;
	public static final int PLAYER_BIT = 2;
	public static final int MONSTER_BIT = 4;
	public static final int MONSTER_PHY_BIT = 8;
	public static final int WIN_BIT = 16;

	public static final float ZOOM_MOD = 0.5f;

	public static SpriteBatch batch;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		setScreen(new GameScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void dispose() {
		super.dispose();
	}
}
