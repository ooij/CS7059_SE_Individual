package com.edwin.maze.Tools;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.edwin.maze.MazeGame;
import com.edwin.maze.Sprites.Monster;
import com.edwin.maze.Sprites.Player;

/**
 * Created by Edwin on 12/11/2016.
 */

public class WorldContactListener implements ContactListener{
    @Override
    public void beginContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();

        int cDef = fixA.getFilterData().categoryBits | fixB.getFilterData().categoryBits;

        switch (cDef){
            case MazeGame.PLAYER_BIT | MazeGame.MONSTER_BIT:
                if(fixA.getFilterData().categoryBits == MazeGame.MONSTER_BIT)
                    ((Monster)fixA.getUserData()).onContact();
                else
                    ((Monster)fixB.getUserData()).onContact();
                break;
            case MazeGame.PLAYER_BIT | MazeGame.WIN_BIT:
                if(fixA.getFilterData().categoryBits == MazeGame.PLAYER_BIT)
                    ((Player)fixA.getUserData()).onEscape();
                else
                    ((Player)fixB.getUserData()).onEscape();
                break;
        }
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
