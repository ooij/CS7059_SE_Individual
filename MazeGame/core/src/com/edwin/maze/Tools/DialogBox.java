package com.edwin.maze.Tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


/**
 * Created by Edwin on 26/11/2016.
 */

public class DialogBox extends Sprite{
    public Skin skin;
    private BitmapFont uiFont;

    public DialogBox() {
        skin = new Skin(Gdx.files.internal("texture/craftacular-ui.json"));

        uiFont = skin.getFont("font");
        uiFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
        uiFont.getData().setScale(0.2f);
        skin.add("uiFont", uiFont);

        Texture dialogTex = new Texture(Gdx.files.internal("texture/dialog.png"));
        skin.add("dialogTex", new NinePatch(dialogTex, 16,16,16,16));

        LabelStyle labelStyle = new LabelStyle(uiFont, Color.WHITE);
        skin.add("uiLabelStyle", labelStyle);
    }

    public void render(Batch batch, float x, float y){
        skin.newDrawable("dialogTex", new Color(1.0f,1.0f,1.0f,0.8f)).draw(batch, x, y, 62, 40);
        uiFont.draw(batch, "WRONG WAY", x+12, y+22);
    }
}
