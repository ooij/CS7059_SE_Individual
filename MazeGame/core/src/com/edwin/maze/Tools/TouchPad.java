package com.edwin.maze.Tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.edwin.maze.MazeGame;


/**
 * Created by Edwin on 13/11/2016.
 */

public class TouchPad {
    Viewport viewport;
    Stage stage;
    boolean up_press, down_press, left_press, right_press;
    OrthographicCamera gameCam;
    private final int key_size =30;

    public TouchPad(SpriteBatch sb){
//        gameCam = new OrthographicCamera();
//        gameCam.setToOrtho(false);
        viewport = new FitViewport(MazeGame.V_WIDTH, MazeGame.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, sb);
        Gdx.input.setInputProcessor(stage);

        Table table = new Table();
        table.setFillParent(true);
        //table.setBounds(-60,20,40,40);
        table.left().bottom();


        final Image upImg = new Image(new Texture("upButton.png"));
        upImg.setSize(key_size, key_size);
        upImg.addListener(new InputListener(){

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                up_press = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                up_press = false;
            }
        });

        final Image downImg = new Image(new Texture("downButton.png"));
        downImg.setSize(key_size, key_size);

        downImg.addListener(new InputListener(){

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                down_press = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                down_press = false;
            }
        });

        final Image leftImg = new Image(new Texture("leftButton.png"));
        leftImg.setSize(key_size, key_size);
        leftImg.addListener(new InputListener(){

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                left_press = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                left_press = false;
            }
        });

        final Image rightImg = new Image(new Texture("rightButton.png"));
        rightImg.setSize(key_size, key_size);
        rightImg.addListener(new InputListener(){

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                right_press = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                right_press = false;
            }
        });

        table.add();
        table.add(upImg).size(upImg.getWidth(), upImg.getHeight());
        table.add();
        table.row().pad(3, 3, 3, 3);
        table.add(leftImg).size(leftImg.getWidth(), leftImg.getHeight());
        table.add();
        table.add(rightImg).size(rightImg.getWidth(), rightImg.getHeight());
        table.row().padBottom(3);
        table.add();
        table.add(downImg).size(downImg.getWidth(), downImg.getHeight());
        table.add();
        //table.setDebug(true);
        stage.addActor(table);
    }

    public void draw(){
        stage.draw();
    }

    public boolean isUp_press() {
        return up_press;
    }

    public boolean isDown_press() {
        return down_press;
    }

    public boolean isLeft_press() {
        return left_press;
    }

    public boolean isRight_press() {
        return right_press;
    }

    public void resize(int width, int height){
        stage.getViewport().update(width, height);
    }
}
