package com.edwin.maze.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.edwin.maze.MazeGame;
import com.badlogic.gdx.graphics.Texture;
import com.edwin.maze.Tools.DialogBox;
import com.edwin.maze.Sprites.Monster;
import com.edwin.maze.Sprites.Player;
import com.edwin.maze.Tools.B2WorldCreator;
import com.edwin.maze.Tools.TouchPad;
import com.edwin.maze.Tools.WorldContactListener;


/**
 * Created by Edwin on 15/10/2016.
 */

public class GameScreen implements Screen {
    private MazeGame game;
    private TextureAtlas atlas;

    private OrthographicCamera gameCam;
    private Viewport gamePort;
    private Player p1;
    private TouchPad touchPad;
    private DialogBox dialogBox;

    private TmxMapLoader mapLoader;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;

    //Box2d
    private World world;
    private Box2DDebugRenderer b2dr;
    private B2WorldCreator creator;

    //light
    private Texture light;
    private FrameBuffer fbo;
    private ShaderProgram lightShader, defaultShader;
    public static final float ambientIntensity = 0.1f;
    public static final Vector3 ambientColor = new Vector3(0.3f, 0.3f, 0.7f);
    final String vertexShader = Gdx.files.internal("texture/vertexShader.glsl").readString();
    final String defaultPixelShader = Gdx.files.internal("texture/defaultPixelShader.glsl").readString();
    final String lightPixelShader =  Gdx.files.internal("texture/pixelShader.glsl").readString();
    private float zAngle;
    private static final float zSpeed = 12.0f;
    private static final float PI2 = 3.1415926535897932384626433832795f * 2.0f;

    //dialog
    private float deltaTime = 100;
    float dx,dy;

    private boolean isGameOver = false;

    public GameScreen(MazeGame game){
        atlas = new TextureAtlas(Gdx.files.internal("texture/characters.pack"));

        this.game = game;
        gameCam = new OrthographicCamera();
        gameCam.setToOrtho(false, MazeGame.V_WIDTH, MazeGame.V_HEIGHT);
        gameCam.zoom = MazeGame.ZOOM_MOD;
        gamePort = new FitViewport(MazeGame.V_WIDTH, MazeGame.V_HEIGHT, gameCam);
        dialogBox = new DialogBox();
        //tile
        mapLoader = new TmxMapLoader();
        map = mapLoader.load("tileset/background.tmx");
        renderer = new OrthogonalTiledMapRenderer(map);
        gameCam.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldHeight() / 2, 0);

        world = new World(new Vector2(0, 0), true);
        b2dr = new Box2DDebugRenderer();

        creator = new B2WorldCreator(this);

        ShaderProgram.pedantic = false;
        defaultShader = new ShaderProgram(vertexShader, defaultPixelShader);
        lightShader = new ShaderProgram(vertexShader, lightPixelShader);

        lightShader.begin();
        lightShader.setUniformf("resolution", Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        lightShader.setUniformi("u_lightmap", 1);
        lightShader.setUniformf("ambientColor", ambientColor.x, ambientColor.y, ambientColor.z, ambientIntensity);
        lightShader.end();

        fbo = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
        light = new Texture(Gdx.files.internal("texture/light.png"));

        p1 = new Player(this);

        touchPad = new TouchPad(game.batch);
        world.setContactListener(new WorldContactListener());
    }

    public TextureAtlas getAtlas(){
        return atlas;
    }

    @Override
    public void show(){
    }

    public void handleInput(float dt){
        gameCam.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
        if(touchPad.isUp_press())
            p1.b2body.applyLinearImpulse(new Vector2(0, 3.0f), p1.b2body.getWorldCenter(), true);
        else if(touchPad.isDown_press())
            p1.b2body.applyLinearImpulse(new Vector2(0, -3.0f), p1.b2body.getWorldCenter(), true);
        else if(touchPad.isLeft_press())
            p1.b2body.applyLinearImpulse(new Vector2(-3.0f, 0), p1.b2body.getWorldCenter(), true);
        else if(touchPad.isRight_press())
            p1.b2body.applyLinearImpulse(new Vector2(3.0f, 0), p1.b2body.getWorldCenter(), true);
        else
            p1.b2body.setLinearVelocity(0,0);
}

    public void update(float dt){
        handleInput(dt);

        world.step(1/60.0f, 6, 2);

        p1.update(dt);
        for (Monster monster : creator.getMonsters())
            monster.update(dt);

        gameCam.position.set(p1.b2body.getPosition().x, p1.b2body.getPosition().y, 0);

        gameCam.update();
        renderer.setView(gameCam);
    }

    @Override
    public void render(float delta) {
        update(delta);

        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.render();

        zAngle += delta * zSpeed;
        while(zAngle > PI2)
            zAngle -= PI2;

        fbo.begin();
        renderer.getBatch().setProjectionMatrix(gameCam.combined);
        renderer.getBatch().setShader(defaultShader);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.getBatch().begin();
        //renderer.getBatch().setBlendFunction(GL20.GL_SRC_COLOR, GL20.GL_SRC_ALPHA);
        float lightSize = 100.0f + 7.0f * (float)Math.sin(zAngle) + 0.2f* MathUtils.random();
        renderer.getBatch().draw(light, gameCam.position.x-lightSize*0.5f, gameCam.position.y-lightSize*0.5f, lightSize, lightSize);
        //renderer.getBatch().setBlendFunction(GL20.GL_SRC_ALPHA,GL20.GL_ONE_MINUS_SRC_ALPHA);
        renderer.getBatch().end();
        fbo.end();

        renderer.getBatch().setProjectionMatrix(gameCam.combined);
        renderer.getBatch().begin();
        if(deltaTime <= 2.0f){
            dialogBox.render(renderer.getBatch(), dx, dy);
            deltaTime += delta;
        }
        renderer.getBatch().setShader(lightShader);
        fbo.getColorBufferTexture().bind(1);
        light.bind(0);
        renderer.getBatch().end();

        game.batch.setProjectionMatrix(gameCam.combined);
        game.batch.begin();
        p1.draw(game.batch);
        game.batch.end();

        //b2dr.render(world, gameCam.combined);

        game.batch.begin();
        game.batch.setShader(lightShader);
        for (Monster monster : creator.getMonsters())
            monster.draw(game.batch);
        game.batch.setShader(defaultShader);
        game.batch.end();

        touchPad.draw();

        if(isGameOver) {
            game.setScreen(new GameOverScreen(game));
            dispose();
        }
    }

    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
        touchPad.resize(width, height);
    }

    public TiledMap getMap(){
        return map;
    }

    public World getWorld(){
        return world;
    }

    public void eventGetLocation(float x, float y){
        deltaTime = 0;
        dx = x-32;
        dy = y+12;
    }

    public void gameOverTrigger(){
        isGameOver = true;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        atlas.dispose();
        map.dispose();
        renderer.dispose();
        world.dispose();
        b2dr.dispose();
        game.dispose();
    }
}
