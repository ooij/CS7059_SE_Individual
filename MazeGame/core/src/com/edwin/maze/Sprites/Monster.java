package com.edwin.maze.Sprites;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.edwin.maze.MazeGame;
import com.edwin.maze.Screen.GameScreen;

/**
 * Created by Edwin on 20/11/2016.
 */

public class Monster extends Sprite{
    public World world;
    public Body b2body;
    private GameScreen screen;

    private float stateTime;
    private Animation monsStand;
    private Array<TextureRegion> frames;

    float mx, my;

    public Monster(GameScreen screen, float x, float y){
        this.screen = screen;
        this.world = screen.getWorld();

        mx = x + 12;
        my = y + 12;
        frames = new Array<TextureRegion>();
        for(int i=0; i<2; i++)
            frames.add(new TextureRegion(screen.getAtlas().findRegion("monster"), i*20, 0, 20, 30));
        monsStand = new Animation(0.33f, frames, Animation.PlayMode.LOOP);
        stateTime = 0;
        setBounds(mx, my, 20, 30);
        defineMonster(mx, my);
    }

    public void update(float dt){
        stateTime +=dt;
        setPosition(b2body.getPosition().x - getWidth() / 2, b2body.getPosition().y - getHeight() / 2);
        setRegion(monsStand.getKeyFrame(stateTime));
    }

    public void defineMonster(float x, float y){
        BodyDef bdef = new BodyDef();
        bdef.position.set(x, y);
        bdef.type = BodyDef.BodyType.StaticBody;
        b2body = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(20, 20);

        fdef.shape = shape;
        fdef.filter.categoryBits = MazeGame.MONSTER_PHY_BIT;
        b2body.createFixture(fdef);

        CircleShape body = new CircleShape();
        body.setRadius(20);
        fdef.shape = body;
        fdef.isSensor = true;
        fdef.filter.categoryBits = MazeGame.MONSTER_BIT;
        fdef.filter.maskBits = MazeGame.PLAYER_BIT;

        b2body.createFixture(fdef).setUserData(this);
    }

    public void onContact(){
        screen.eventGetLocation(b2body.getPosition().x, b2body.getPosition().y);
    }
}
