package com.edwin.maze.Sprites;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.edwin.maze.MazeGame;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.edwin.maze.Screen.GameScreen;

/**
 * Created by Edwin on 10/11/2016.
 */

public class Player extends Sprite{
    public enum State{STANDING, WALK_UP, WALK_DOWN, WALK_LEFT, WALK_RIGHT};
    public State currentState;
    public State previousState;
    public World world;
    private GameScreen screen;
    public Body b2body;
    private Array<TextureRegion> charStand;
    private TextureRegion region;
    private Animation walk_up, walk_down, walk_left, walk_right;

    private float stateTimer;
    private boolean walking;

    public Player(GameScreen screen){
        super(screen.getAtlas().findRegion("character"));
        this.screen = screen;
        this.world = screen.getWorld();
        currentState = State.STANDING;
        previousState = State.STANDING;
        stateTimer = 0;
        walking = true;

        Array<TextureRegion> frame = new Array<TextureRegion>();

        for(int i=1; i<3; i++)
            frame.add(new TextureRegion(getTexture(), i*20, 0, 20, 32));
        walk_down = new Animation(0.25f, frame, Animation.PlayMode.LOOP);
        frame.clear();

        for(int i=4; i<6; i++)
            frame.add(new TextureRegion(getTexture(), i*20, 0, 20, 32));
        walk_up = new Animation(0.25f, frame, Animation.PlayMode.LOOP);
        frame.clear();

        for(int i=7; i<9; i++)
            frame.add(new TextureRegion(getTexture(), i*20, 0, 20, 32));
        walk_left = new Animation(0.25f, frame, Animation.PlayMode.LOOP);
        frame.clear();

        for(int i=10; i<12; i++)
            frame.add(new TextureRegion(getTexture(), i*20, 0, 20, 32));
        walk_right = new Animation(0.25f, frame, Animation.PlayMode.LOOP);
        frame.clear();

        charStand = new Array<TextureRegion>();
        for(int i=0; i<11; i+=3)
            charStand.add(new TextureRegion(getTexture(), i*20, 0, 20, 32));

        definePlayer();

        setBounds(0, 0, 18, 27);
        setRegion(charStand.get(0));
    }

    public void update(float dt){
        setPosition(b2body.getPosition().x - getWidth() / 2, b2body.getPosition().y - getHeight() / 2);
        setRegion(getFrame(dt));
    }

    public TextureRegion getFrame(float dt){
        currentState = getState();

        switch(currentState){
            case WALK_RIGHT:
                region = walk_right.getKeyFrame(stateTimer);
                previousState = currentState;
                break;
            case WALK_LEFT:
                region = walk_left.getKeyFrame(stateTimer);
                previousState = currentState;
                break;
            case WALK_UP:
                region = walk_up.getKeyFrame(stateTimer);
                previousState = currentState;
                break;
            case WALK_DOWN:
                region = walk_down.getKeyFrame(stateTimer);
                previousState = currentState;
                break;
            case STANDING:
                switch(previousState){
                    case WALK_RIGHT:
                        region = charStand.get(3);
                        previousState = State.WALK_RIGHT;
                        break;
                    case WALK_LEFT:
                        region = charStand.get(2);
                        previousState = State.WALK_LEFT;
                        break;
                    case WALK_UP:
                        region = charStand.get(1);
                        previousState = State.WALK_UP;
                        break;
                    case WALK_DOWN:
                    default:
                        region = charStand.get(0);
                        previousState = State.STANDING;
                        break;
                }
                break;
        }

        stateTimer = (currentState == previousState) ? stateTimer + dt : 0;


        return region;
    }

    public State getState(){
        if(b2body.getLinearVelocity().x > 0 && b2body.getLinearVelocity().y==0)
            return State.WALK_RIGHT;
        else if(b2body.getLinearVelocity().x < 0 && b2body.getLinearVelocity().y==0)
            return State.WALK_LEFT;
        else if(b2body.getLinearVelocity().y > 0 && b2body.getLinearVelocity().x==0)
            return State.WALK_UP;
        else if(b2body.getLinearVelocity().y < 0 && b2body.getLinearVelocity().x==0)
            return State.WALK_DOWN;
        else
            return State.STANDING;
    }

    public void definePlayer(){
        BodyDef bdef = new BodyDef();
        bdef.position.set(300, 450);
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body = world.createBody(bdef);
        b2body.setLinearDamping(3.0f);

        FixtureDef fdef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(7, 7, new Vector2(0, -5), 0);
        fdef.shape = shape;
        b2body.createFixture(fdef);

        PolygonShape body = new PolygonShape();
        body.setAsBox(22, 22, new Vector2(0, -5), 0);
        fdef.shape = body;
        fdef.isSensor = true;
        fdef.filter.categoryBits = MazeGame.PLAYER_BIT;
        fdef.filter.maskBits = MazeGame.MONSTER_BIT | MazeGame.WIN_BIT;

        b2body.createFixture(fdef).setUserData(this);
    }

    public void onEscape(){
        screen.gameOverTrigger();
    }
}
